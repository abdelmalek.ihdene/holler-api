import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.commentTableName,
    Key: {
      postId: data.postId,
      commentId: data.commentId
    },
    ConditionExpression: "userId = :userIdVal",
    UpdateExpression: "set content = :contentVal",
    ExpressionAttributeValues: {
      ":contentVal": data.content,
      ":userIdVal": event.requestContext.identity.cognitoIdentityId
    },
  };
  await dynamoDb.update(params);
  return `Success: update-comment(${data.postId}, ${data.commentId})`;
});
