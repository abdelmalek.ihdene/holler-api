import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const params = {
    TableName: process.env.postTableName,
    ProjectionExpression: "content, createdAt, postId",
    KeyConditionExpression: "username = :usernameVal",
    ExpressionAttributeValues: {
      ":usernameVal": event.pathParameters.username
    }
  };
  const result = await dynamoDb.query(params);
  return result;
});
