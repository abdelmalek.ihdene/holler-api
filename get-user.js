import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  console.log(event.pathParameters.username);
  const params = {
    TableName: process.env.userTableName,
    ProjectionExpression: "bio, createdAt, profileImageKey, userLocation, website",
    KeyConditionExpression: "username = :usernameVal",
    ExpressionAttributeValues: {
      ":usernameVal": event.pathParameters.username
    }
  };
  const result = await dynamoDb.query(params);
  return result;
});
