import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.postTableName,
    Key: {
      username: data.username,
      postId: data.postId
    },
    ConditionExpression: "userId = :userIdVal",
    ExpressionAttributeValues: {
      ":userIdVal": event.requestContext.identity.cognitoIdentityId
    }
  };
  await dynamoDb.delete(params);
  return `Success: delete-post(${data.username}, ${data.postId})`;
});
