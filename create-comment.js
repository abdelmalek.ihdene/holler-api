import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
import * as uuid from "uuid";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.commentTableName,
    Item: {
      commentId: uuid.v1(),
      content: data.content,
      createdAt: Date.now(),
      postId: data.postId,
      userId: event.requestContext.identity.cognitoIdentityId,
      username: data.username
    },
  };
  await dynamoDb.put(params);
  return params.Item;
});
