import { buildUpdateExpression } from "./libs/utility-lib";
import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const validExpressionAttributes =
  ["bio", "profileImageKey", "userLocation", "website"];
  let [updateExpression, expressionAttributeValues] =
  buildUpdateExpression(validExpressionAttributes, data);
  if(Object.keys(expressionAttributeValues).length == 0) {
    throw new Error("The conditional request failed");
  }
  expressionAttributeValues[":userIdVal"] =
  event.requestContext.identity.cognitoIdentityId;
  console.log(updateExpression, expressionAttributeValues);
  const params = {
    TableName: process.env.userTableName,
    Key: {
      username: data.username
    },
    ConditionExpression: "userId = :userIdVal",
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
    ReturnValues: "ALL_NEW"
  };
  const result = await dynamoDb.update(params);
  return result;
});
