import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const params = {
    TableName: process.env.likeTableName,
    ProjectionExpression: "username",
    KeyConditionExpression: "postId = :postIdVal",
    ExpressionAttributeValues: {
      ":postIdVal": event.pathParameters.postId
    }
  };
  const result = await dynamoDb.query(params);
  return result;
});
