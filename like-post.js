import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.likeTableName,
    Item: {
      postId: data.postId,
      username: data.username
    }
  };
  await dynamoDb.put(params);
  return params.Item;
});
