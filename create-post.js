import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
import * as uuid from "uuid";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.postTableName,
    Item: {
      content: data.content,
      createdAt: Date.now(),
      postId: uuid.v1(),
      userId: event.requestContext.identity.cognitoIdentityId,
      username: data.username
    }
  };
  await dynamoDb.put(params);
  return params.Item;
});
