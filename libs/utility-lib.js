export function buildUpdateExpression(validExpressionAttributes, data) {
  const dataLength = Object.keys(data).length;
  let updateExpression = "set ";
  let expressionAttributeValues = {};
  for(let i = 0; i < validExpressionAttributes.length; i += 1) {
    let attribute = validExpressionAttributes[i];
    if(attribute in data) {
      let valueKey = ":" + attribute + "Val";
      expressionAttributeValues[valueKey] = data[attribute];
      updateExpression += attribute + " = " + valueKey;
      if(i < validExpressionAttributes.length - 1 && i < dataLength - 1) {
        updateExpression += ", ";
      }
    }
  }
  return [updateExpression, expressionAttributeValues];
};
