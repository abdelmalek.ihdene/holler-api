import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.likeTableName,
    Key: {
      username: data.username,
      postId: data.postId
    }
  };
  await dynamoDb.delete(params);
  return `Success: unlike-post(${data.username}, ${data.postId})`;
});
