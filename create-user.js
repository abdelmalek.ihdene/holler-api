import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.userTableName,
    Item: {
      createdAt: Date.now(),
      userId: event.requestContext.identity.cognitoIdentityId,
      username: data.username
    },
    ConditionExpression: 'attribute_not_exists(username)',
  };
  await dynamoDb.put(params);
  return `Success: create-user(${params.Item.userId}, ${data.username})`;
});
