import dynamoDb from "./libs/dynamodb-lib";
import handler from "./libs/handler-lib";
export const main = handler(async (event, context) => {
  const data = JSON.parse(event.body);
  const params = {
    TableName: process.env.postTableName,
    Key: {
      username: data.username,
      postId: data.postId
    },
    ConditionExpression: "userId = :userIdVal",
    UpdateExpression: "set content = :contentVal",
    ExpressionAttributeValues: {
      ":contentVal": data.content,
      ":userIdVal": event.requestContext.identity.cognitoIdentityId
    },
  };
  await dynamoDb.update(params);
  return `Success: update-post(${data.postId}, ${data.username})`;
});
