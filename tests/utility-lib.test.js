import { buildUpdateExpression } from "../libs/utility-lib";
test("buildUpdateExpression <1: 3 -> 3>", () => {
  // Input variables
  const validExpressionAttributes = ["a", "b", "c"];
  const data = {
    a: 1,
    b: 2,
    c: 3
  };
  // Expected output variables
  const expectedUpdateExpression = "set a = :aVal, b = :bVal, c = :cVal";
  const expectedExpressionAttributeValues = {
    ":aVal": 1,
    ":bVal": 2,
    ":cVal": 3,
  };
  const [updateExpression, expressionAttributeValues] =
  buildUpdateExpression(validExpressionAttributes, data);
  expect(updateExpression).toEqual(expectedUpdateExpression);
  expect(expressionAttributeValues).toEqual(expectedExpressionAttributeValues);
});
test("buildUpdateExpression <2: 3 -> 2>", () => {
  // Input variables
  const validExpressionAttributes = ["a", "b", "c"];
  const data = {
    a: 1,
    b: 2
  };
  // Expected output variables
  const expectedUpdateExpression = "set a = :aVal, b = :bVal";
  const expectedExpressionAttributeValues = {
    ":aVal": 1,
    ":bVal": 2,
  };
  const [updateExpression, expressionAttributeValues] =
  buildUpdateExpression(validExpressionAttributes, data);
  expect(updateExpression).toEqual(expectedUpdateExpression);
  expect(expressionAttributeValues).toEqual(expectedExpressionAttributeValues);
});
test("buildUpdateExpression <3: 2 -> 3>", () => {
  // Input variables
  const validExpressionAttributes = ["a", "b"];
  const data = {
    a: 1,
    b: 2,
    c: 3,
  };
  // Expected output variables
  const expectedUpdateExpression = "set a = :aVal, b = :bVal";
  const expectedExpressionAttributeValues = {
    ":aVal": 1,
    ":bVal": 2,
  };
  const [updateExpression, expressionAttributeValues] =
  buildUpdateExpression(validExpressionAttributes, data);
  expect(updateExpression).toEqual(expectedUpdateExpression);
  expect(expressionAttributeValues).toEqual(expectedExpressionAttributeValues);
});
